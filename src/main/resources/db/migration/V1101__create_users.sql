use `users`;

CREATE TABLE `users` (
 `id` INT auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
);