'use strict';
window.onload = preloadMessages();


function preloadMessages() {
    setInterval(preloadWindow, 1000);
}

async function getMessage() {
    let user = document.getElementById("username").textContent;
    let url = "http://localhost:8080/messages/"+user;
    let response = await fetch(url);
    if(response!=null) {
        return await response.json();
    }
    return null;
}

class Message{
    constructor(id, sender, content) {
        this.id = id;
        this.sender = sender;
        this.content = content;
    }
}


    async function preloadWindow() {
    let messages = await getMessage();
    if(messages!=null) {
        let newMessage = new Message(messages.id, messages.sender, messages.content);
        let elem = createMessage(newMessage);
        document.getElementById("chat-body").appendChild(elem);
    }
}

function createMessage(newMessage){
    let message_content = `<div id = "${newMessage.id}">
                            <div>
                                <h5>${newMessage.sender}</h5>
                            </div>
                            <div>
                                <p>${newMessage.content}</p>
                            </div>
                        </div>
                        `
    let element = document.createElement(`div`);
    element.innerHTML += message_content;
    return element;
}

function pushMessage(form) {
    let data = new FormData(form);
    let user_id = data.get("user_id").toString();

    fetch("http://localhost:8080/chat/"+user_id, {
        method: "POST",
        body: data
    }).then(r => r.json()).then(data => {
        console.log(data);
    });
}