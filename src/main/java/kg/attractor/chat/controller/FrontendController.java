package kg.attractor.chat.controller;

import kg.attractor.chat.model.Message;
import kg.attractor.chat.model.User;
import kg.attractor.chat.repository.UserRepository;
import kg.attractor.chat.service.MessageService;
import kg.attractor.chat.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@AllArgsConstructor
@Controller
public class FrontendController {
    private final UserService userService;
    private final UserRepository userRepository;
    private final MessageService messageService;

    @GetMapping
    public String index(Model model, HttpSession session){
        if(!userService.isThisUserExist(session)){
            userService.createNewUser(session);
        }
        List<User> users = userService.getUsers(session);
        if(users!=null) {
            model.addAttribute("users", users);
        }
        return "index";
    }

    @GetMapping("/chat/{id:\\d+?}")
    public String chatPage(@PathVariable int id, Model model){
        model.addAttribute("user", userRepository.findUserById(id));
        return "chat-page";
    }

    @RequestMapping(value = "/chat/{id:\\d+?}", method = RequestMethod.POST)
    public void createMessage(@PathVariable int id, HttpSession session, @RequestParam("message") String message){
        messageService.createNewMessage(id, session, message);
    }

    @GetMapping("/messages/{username}")
    @ResponseBody
    public Message returnMessages(@PathVariable String username, HttpSession session){
        return messageService.getMessage(username, session);
    }
}
