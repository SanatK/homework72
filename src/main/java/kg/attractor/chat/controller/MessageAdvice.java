package kg.attractor.chat.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class MessageAdvice {

    @ModelAttribute(Constants.MESSAGE_ID)
    public List<String> getMessages(HttpSession session){
        var list = session.getAttribute(Constants.MESSAGE_ID);
        if(list == null) {
            session.setAttribute(Constants.MESSAGE_ID, new ArrayList<>());
        }
        return (List<String>) session.getAttribute(Constants.MESSAGE_ID);
    }
}
