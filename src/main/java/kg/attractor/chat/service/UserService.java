package kg.attractor.chat.service;

import kg.attractor.chat.model.User;
import kg.attractor.chat.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

@AllArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;

    public boolean isThisUserExist(HttpSession session){
        List<User> users = userRepository.findAll();
        for(User u: users){
            if(u.getName().equals(session.getId())){
                return true;
            }
        }
        return false;
    }

    public void createNewUser(HttpSession session){
        userRepository.save(
        User.builder()
                .name(session.getId())
                .build());
    }

    public List<User> getUsers(HttpSession session){
        List<User> users = userRepository.findAll();
        for(int i = 0; i<users.size(); i++){
            if(users.get(i).getName().equals(session.getId())){
                users.remove(i);
            }
        }
        return users;
    }

}
