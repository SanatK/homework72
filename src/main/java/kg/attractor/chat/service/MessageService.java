package kg.attractor.chat.service;

import kg.attractor.chat.model.Message;
import kg.attractor.chat.repository.MessageRepository;
import kg.attractor.chat.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpSession;
import java.util.List;

@AllArgsConstructor
@Service
public class MessageService {
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;


    public Message createNewMessage(Integer id, HttpSession session, String message){
            return messageRepository.save(
                    Message.builder()
                    .sender(session.getId())
                    .receiver(userRepository.findUserById(id).getName())
                    .content(message)
                    .build());
    }

    public Message getMessage(String username, HttpSession session){
        List<Message> messages = messageRepository.findAllBySender(username);
        if(messages!=null){
            for(Message m: messages){
                if(m.getReceiver().equals(session.getId())&&!m.isDelivered()){
                    m.setDelivered(true);
                    messageRepository.save(m);
                    return m;
                }
            }
        }
        return null;
    }

}
