package kg.attractor.chat.repository;

import kg.attractor.chat.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    List<User> findAll();
    User findUserById(Integer id);
}
